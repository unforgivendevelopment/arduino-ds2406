# **arduino-ds2406** library #

This is an Arduino Library for the _**[DS2406][1]** OneWire device_.



## Resources ##

* [**DS2406** Product Page][1]
* [**DS2406** Datasheet][2]
* [**MAXIM Application Note 5856** -- _DS2406 PIO Command Examples_][3]












[1]:	<https://www.maximintegrated.com/en/products/digital/memory-products/DS2406.html>
[2]:	<https://datasheets.maximintegrated.com/en/ds/DS2406.pdf>
[3]:	<https://www.maximintegrated.com/en/app-notes/index.mvp/id/5856>
