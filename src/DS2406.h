/**
 * \file DS2406.h
 * The header file defining the core functionality of the DS2406 1-Wire device driver library; the actual functionality
 * is implemented in DS2406.cpp
 *
 * \brief Defines the structure and functionality of the DS2406 device driver implementation
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017-05-17
 * \copyright	BSD license
 */

#ifndef _SRC_DS2406_H__
#define _SRC_DS2406_H__

#include <OneWire.h>

/**
 * \name DS2406 Definitions
 * A bunch of constant values which are used by the DS2406
 */

/**
 * @{
 */

/**
 *
 * \def OW_FAMILY_CODE
 * Defines the first octet of the OneWire serial number, which is the 1-Wire device family code.
 * \def OW_CMD_WRITE_STATUS
 * Defines the command to write the status register.
 * \def OW_CMD_READ_STATUS
 * Defines the command to read the status register.
 * \def OW_CMD_READ_MEMORY
 * Defines the command to read data from the EPROM memory array.
 * \def OW_CMD_EXTENDED_READ_MEMORY
 * Defines the command to read data from the EPROM memory array, with some enhanced functionality and features.
 * \def OW_CMD_WRITE_MEMORY
 * Defines the command to write data to the EPROM memory array.
 * \warning WRITING DATA TO THE EPROM ON THIS DEVICE REQUIRES VERY SPECIFIC HARDWARE CONSIDERATIONS.\n
 * Please see the DS2406 datasheet for additional information.
 * \def OW_CMD_CHANNEL_ACCESS
 * Defines the command used to access the PIO channels:\n
 * To sense the logical status of the output node and transistor, and to change the status of the output transistor.
 * \def OW_CH_ACC_REG_BV_CRC0
 * Defines the bit value of CRC0 (CRC control, bit 0).
 * \def OW_CH_ACC_REG_BV_CRC1
 * Defines the bit value of CRC1 (CRC control, bit 1).
 * \def OW_CH_ACC_REG_BV_CHS0
 * Defines the bit value of CHS0 (Channel Select 0 - PIO_A).
 * \def OW_CH_ACC_REG_BV_CHS1
 * Defines the bit value of CHS1 (Channel Select 1 - PIO_B).
 * \def OW_CH_ACC_REG_BV_IC
 * Defines the bit value of IC (Interleave Control).
 * \def OW_CH_ACC_REG_BV_TOG
 * Defines the bit value of TOG
 * \def OW_CH_ACC_REG_BV_IM
 * Defines the bit value of IM
 * \def OW_CH_ACC_REG_BV_ALR
 * Defines the bit value of ALR
 * \def OW_CH_ACC_REG_BM_SELECT_PIO_A
 * Defines the bitmask to select PIO A for the channel access operation.
 * \def OW_CH_ACC_REG_BM_SELECT_PIO_B
 * Defines the bitmask to select PIO B for the channel access operation.
 * \def OW_CH_ACC_REG_BM_SELECT_PIO_AB
 * Defines the bitmask to select both PIO A & B for the channel access operation.
 * \def OW_CH_ACC_REG_BM_DISABLE_CRC
 * Defines the bitmask to disable CRC generation for channel access data packets.
 * \def OW_CH_ACC_REG_BM_CRC_EA_1_BYTE
 * Defines the bitmask to enable CRC generation for each byte of channel access data packets.
 * \def OW_CH_ACC_REG_BM_CRC_EA_8_BYTE
 * Defines the bitmask to enable CRC generation for each 8 bytes of channel access data packets.
 * \def OW_CH_ACC_REG_BM_CRC_EA_32_BYTE
 * Defines the bitmask to enable CRC generation for each 32 bytes of channel access data packets.
 * \def OW_CH_ACC_REG_BM_SINGLE_CH_ACC
 * Defines the bitmask for the interleave control setting while using a single PIO.
 * \def OW_CH_ACC_REG_BM_2CH_ASYNC_ACC
 * Defines the bitmask for the interleave control setting for asynchronous access while controlling both PIOs.
 * \def OW_CH_ACC_REG_BM_2CH_SYNC_ACC
 * Defines the bitmask for the interleave control setting for synchronous access while controlling both PIOs.
 * \def OW_CH_ACC_REG_CONTROL_BYTE_2
 * Defines the constant value for which the second channel access control byte will always be.
 * \def OW_DS2406_STATUS_BUFLEN
 * Defines the size of the DS2406's status buffer.
 */
#define OW_FAMILY_CODE					0x12
#define OW_CMD_WRITE_STATUS				0x55
#define OW_CMD_READ_STATUS				0xAA
#define OW_CMD_READ_MEMORY				0xF0
#define OW_CMD_EXTENDED_READ_MEMORY		0xA5
#define OW_CMD_WRITE_MEMORY				0x0F
#define OW_CMD_CHANNEL_ACCESS			0xF5
#define OW_CH_ACC_REG_BV_CRC0			0x01
#define OW_CH_ACC_REG_BV_CRC1			0x02
#define OW_CH_ACC_REG_BV_CHS0			0x04
#define OW_CH_ACC_REG_BV_CHS1			0x08
#define OW_CH_ACC_REG_BV_IC				0x10
#define OW_CH_ACC_REG_BV_TOG			0x20
#define OW_CH_ACC_REG_BV_IM				0x40
#define OW_CH_ACC_REG_BV_ALR			0x80
#define OW_CH_ACC_REG_BM_SELECT_PIO_A	0x04
#define OW_CH_ACC_REG_BM_SELECT_PIO_B	0x08
#define OW_CH_ACC_REG_BM_SELECT_PIO_AB	0x0C
#define OW_CH_ACC_REG_BM_DISABLE_CRC	0x00
#define OW_CH_ACC_REG_BM_CRC_EA_1_BYTE	0x01
#define OW_CH_ACC_REG_BM_CRC_EA_8_BYTE	0x02
#define OW_CH_ACC_REG_BM_CRC_EA_32_BYTE	0x03
#define OW_CH_ACC_REG_BM_SINGLE_CH_ACC	0x00
#define OW_CH_ACC_REG_BM_2CH_ASYNC_ACC	0x00
#define OW_CH_ACC_REG_BM_2CH_SYNC_ACC	0x10
#define OW_CH_ACC_REG_CONTROL_BYTE_2	0xFF
#define OW_DS2406_STATUS_BUFLEN			8

/**
 * @}
 */




/**
 * \class DS2406
 * Provides an interface to the DS2406, the "Dual Addressable Switch Plus 1Kb Memory" 1-Wire device, manufactured by
 * Maxim (orignally Dallas).
 * \brief Provides an interface to DS2406 devices on a 1-Wire network
 */
class DS2406 {
public:
	/**
	 * The constructor for a DS2406 object. Creates a new instance of a DS2406 device, which is able to be controlled
	 * independently of other devices on the 1-Wire bus.
	 *
	 * \brief The constructor for a DS2406 object; creates a new DS2406 object instance.
	 *
	 * \param[in]	bus			A reference to the OneWire bus instance the DS2406 is physically attached to.
	 * \param[in]	owDevAddr	An 8-byte array of unsigned integers (uint8_t) representing the 1-Wire serial number of
	 *							the DS2406 device to instantiate an object for.
	 */
	DS2406(OneWire *bus, uint8_t *owDevAddr);

	bool readPioState(int pio = PIO_A);
	void writePioState(bool pio_a, bool pio_b = false);

private:
	OneWire *owBus;
	uint8_t deviceAddress[8];

	void readStatus(uint8_t *buf);

}


#endif	/* _SRC_DS2406_H__ */
