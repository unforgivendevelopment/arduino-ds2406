/**
 * \file DS2406.cpp
 * Provides the core functionality of the DS2406 1-Wire device driver library.
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017-05-17
 * \copyright	BSD license
 */


#include "DS2406.h"



bool DS2406::readPioState(int pio = PIO_A) {
	/* do stuff */
}


void DS2406::writePioState(bool pio_a, bool pio_b = false) {
	/* do stuff */
}
