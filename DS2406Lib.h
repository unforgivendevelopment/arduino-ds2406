/**
 * ---------------------------------------------------------------------------------------------------------------------
 * \file DS2406Lib.h	A "wrapper" for the DS2406 library.
 *						No "actual" functionality is provided by this file; it simply includes 'src/DS2406.h'
 *
 * \author		Gerad Munsch <gmunsch@unforgivendevelopment.com>
 * \date		2017-05-17
 * \copyright	BSD license
 * ---------------------------------------------------------------------------------------------------------------------
 */

#ifndef _DS2406LIB_H__
#define _DS2406LIB_H__

#include "src/DS2406.h"

#endif	/* _DS2406LIB_H__ */
